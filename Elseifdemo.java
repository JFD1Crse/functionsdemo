
package elseifdemo;


public class Elseifdemo {

    
    public static void main(String[] args) {
       int percentage=50;
       if(percentage>75){
           System.out.println("Distinction");
       }
       else if(percentage>60){
           System.out.println("Second class");
       }
       else if(percentage>50){
           System.out.println("Third class");
       }
        
    }
    
}
